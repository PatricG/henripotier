package fr.xebia.henripotier.book.repository

import fr.xebia.henripotier.book.model.Book
import fr.xebia.henripotier.book.model.Offer
import kotlinx.coroutines.flow.Flow

interface BookRepository {
    fun getBooks() : Flow<List<Book>>
    fun selectBookIsbn(isbn : String)
    fun getSelectBook() : List<Book>
    fun getSelectBookIsbn() : List<String>
    fun unSelectBookIsbn(isbn : String)
    fun getCommercialOffers(isbns : List<String>) : Flow<List<Offer>>
}
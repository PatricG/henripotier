package fr.xebia.henripotier.book.model

data class Offer(
    val sliceValue: Float?,
    val type: String,
    val value: Float
)
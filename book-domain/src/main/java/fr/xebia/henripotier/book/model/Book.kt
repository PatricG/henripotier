package fr.xebia.henripotier.book.model

data class Book(
    val isbn: String,
    val title: String,
    val cover: String,
    val price: Float,
    val synopsis: List<String>
){
    fun getDescription() : String = synopsis.first()
}
package fr.xebia.henripotier.book.domain

import fr.xebia.henripotier.book.model.Offer


fun List<Offer>.getBestOffer(prices: List<Float>): Float {
    var total = 0.0f
    var result = 0.0f
    prices.forEach {
        total += it
    }

    result = total
    this.forEach {
        if (it.type == "minus") {
            val price = total - it.value
            if (price < result) {
                result = price
            }
        }
        if (it.type == "percentage") {
            val price = total * (100 - it.value) / 100
            if (price < result) {
                result = price
            }
        }
        if (it.type == "sliceValue") {
            it.sliceValue?.also { sliceValue ->
                if (total > sliceValue) {
                    val price = total - it.value
                    if (price < result) {
                        result = price
                    }
                }
            }
        }
    }

    return result
}


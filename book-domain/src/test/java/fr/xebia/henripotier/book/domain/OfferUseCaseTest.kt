package fr.xebia.henripotier.book.domain


import fr.xebia.henripotier.book.model.Offer
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class OfferUseCaseTest {

    @Test
    fun test_getBestOffer() {
        listOf(
            DataOfferTest( // empty offeere
                listOf(35f, 30f),
                listOf(),
                bestOffer = 65f
            ),
            DataOfferTest( // use demandé
                listOf(35f, 30f),
                listOf(
                    Offer(
                        type = "percentage",
                        value = 5f,
                        sliceValue = null
                    ),
                    Offer(
                        type = "minus",
                        value = 15f,
                        sliceValue = null
                    ),
                    Offer(
                        type = "slice",
                        value = 12f,
                        sliceValue = 100f
                    )
                ),
                bestOffer = 50f
            )
        ).forEach { data ->
            assertEquals(data.offers.getBestOffer(data.prices), data.bestOffer)
        }
    }
}

data class DataOfferTest(val prices: List<Float>, val offers: List<Offer>, val bestOffer: Float)
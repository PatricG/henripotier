package fr.xebia.henripotier.book

import fr.xebia.henripotier.book.data.DataFactory
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val dataModule = module {

    single {
        DataFactory.createBookDataBase(androidContext())
    }

    single {
        DataFactory.createBookService()
    }

    single {
        DataFactory.createBookRepository(get(), get())
    }
}

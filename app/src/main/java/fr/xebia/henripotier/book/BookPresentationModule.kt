package fr.xebia.henripotier.book

import fr.xebia.henripotier.book.presentation.ListBookViewModel
import fr.xebia.henripotier.book.presentation.OfferBookViewModel
import org.koin.dsl.module

val presentationModule = module {

    factory {
        ListBookViewModel(get())
    }

    factory {
        OfferBookViewModel(get())
    }

}
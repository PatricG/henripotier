package fr.xebia.henripotier.book

import android.app.Application
import androidx.lifecycle.ViewModel
import fr.xebia.henripotier.book.presentation.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BookApplication : Application(), BaseActivity.ViewModelInjector {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@BookApplication)
            modules(dataModule, presentationModule)
        }
    }

    override fun injectViewModel(activity: BaseActivity) : ViewModel? {
        return when(activity){
            is ListBookActivity -> inject<ListBookViewModel>().value
            is OfferBookActivity -> inject<OfferBookViewModel>().value
            else -> null
        }
    }
}


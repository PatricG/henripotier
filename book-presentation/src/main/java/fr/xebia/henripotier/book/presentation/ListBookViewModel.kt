package fr.xebia.henripotier.book.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.xebia.henripotier.book.model.Book
import fr.xebia.henripotier.book.repository.BookRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ListBookViewModel(private val repository: BookRepository) : ViewModel() {

    private val _listBooks = MutableLiveData<List<Book>>()

    private val _showProgressBar = MutableLiveData<Boolean>().apply {
        value = false
    }

    val showProgressBar : LiveData<Boolean> get() = _showProgressBar

    val listBooks : LiveData<List<Book>> get() = _listBooks

    fun fetchBooks() {
        viewModelScope.launch {
            repository.getBooks().collect {
                _listBooks.value = it
            }
        }
    }

    fun onChecked(isbn : String, checked : Boolean){
        if(checked){
            repository.selectBookIsbn(isbn)
        }else {
            repository.unSelectBookIsbn(isbn)
        }
    }
}
package fr.xebia.henripotier.book.presentation

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import fr.xebia.henripotier.book.presentation.databinding.ActivityListBookBinding


class ListBookActivity : BaseActivity() {

    private var listBookViewModel : ListBookViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ActivityListBookBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_list_book)

        listBookViewModel = injectViewModel(this)

        listBookViewModel?.apply {
            setBinding(binding, this)
            fetchBooks()
        }
    }

    private fun setBinding(binding : ActivityListBookBinding, viewModel: ListBookViewModel){
        binding.listBookViewModel = viewModel
        binding.booksRecyclerView.adapter =  ListBookAdapter(this@ListBookActivity, viewModel)
        binding.panier.setOnClickListener {
            startActivity(OfferBookActivity.newIntent(this))
        }
    }

}
package fr.xebia.henripotier.book.presentation

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel


open class BaseActivity : AppCompatActivity() {

    protected fun <VM : ViewModel, A : BaseActivity>injectViewModel(activity : A) : VM? {
        return (applicationContext as? ViewModelInjector)?.injectViewModel(activity) as? VM
    }

    interface ViewModelInjector {
        fun injectViewModel(activity : BaseActivity) : ViewModel?
    }

}
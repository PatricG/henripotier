package fr.xebia.henripotier.book.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import fr.xebia.henripotier.book.presentation.databinding.ActivityOfferBookBinding

class OfferBookActivity : BaseActivity() {

    companion object {
        fun newIntent(context : Context) = Intent(context, OfferBookActivity::class.java)
    }

    private var offerBookViewModel : OfferBookViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ActivityOfferBookBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_offer_book)

        offerBookViewModel = injectViewModel(this)

        offerBookViewModel?.apply {
            binding.offerBookViewModel = this
            binding.lifecycleOwner = this@OfferBookActivity
            fetchBestOffer()
        }
    }
}
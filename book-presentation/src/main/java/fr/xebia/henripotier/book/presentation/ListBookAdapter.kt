package fr.xebia.henripotier.book.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.xebia.henripotier.book.model.Book
import fr.xebia.henripotier.book.presentation.databinding.ItemListBookBinding


class ListBookAdapter(lifecycleOwner: LifecycleOwner, private val viewModel: ListBookViewModel) :
    RecyclerView.Adapter<ListBookAdapter.BookViewHolder>() {

    private val listBooks: MutableList<Book> = mutableListOf()

    init {
        viewModel.listBooks.observe(lifecycleOwner, Observer {
            listBooks.clear()
            listBooks += it
            notifyDataSetChanged()
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListBookBinding.inflate(inflater, parent, false)
        return BookViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val book: Book = listBooks[position]
        holder.binding.item = book
        holder.binding.choose.setOnCheckedChangeListener { _, checked ->
            viewModel.onChecked(book.isbn, checked)
        }
    }

    override fun getItemCount(): Int = listBooks.size

    class BookViewHolder(val binding: ItemListBookBinding) : RecyclerView.ViewHolder(binding.root)

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String) {
            Glide.with(view.context).load(url).into(view)
        }
    }
}


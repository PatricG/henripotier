package fr.xebia.henripotier.book.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.xebia.henripotier.book.domain.getBestOffer
import fr.xebia.henripotier.book.repository.BookRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class OfferBookViewModel(private val repository: BookRepository) : ViewModel() {

    private val _bestOffer = MutableLiveData<String>().apply {
        value = "0n"
    }

    private val _showProgressBar = MutableLiveData<Boolean>().apply {
        value = false
    }

    val showProgressBar: LiveData<Boolean> get() = _showProgressBar

    val bestOffer: LiveData<String> get() = _bestOffer

    fun fetchBestOffer() {

        viewModelScope.launch {
            val selectedIsbns = repository.getSelectBookIsbn()
            val prices = repository.getSelectBook().map { it.price }
            repository.getCommercialOffers(selectedIsbns).collect {
                _bestOffer.value = it.getBestOffer(prices).toString()
            }
        }
    }
}
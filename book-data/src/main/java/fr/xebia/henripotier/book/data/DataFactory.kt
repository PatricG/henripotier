package fr.xebia.henripotier.book.data

import android.content.Context
import androidx.room.Room
import fr.xebia.henripotier.book.repository.BookRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DataFactory {
    private const val BASE_URL =  "http://henri-potier.xebia.fr/"
    private const val DATA_BASE_NAME =  "database-book"

    fun createBookService() : BookService {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create<BookService>(BookService::class.java)
    }

    fun createBookDataBase(context: Context) : BookDataBase =
        Room.databaseBuilder(
            context,
            BookDataBase::class.java, DATA_BASE_NAME
        ).build()

    fun createBookRepository(bookService: BookService, bookDataBase: BookDataBase) :
            BookRepository = BookRepositoryImpl(bookService, bookDataBase)

}
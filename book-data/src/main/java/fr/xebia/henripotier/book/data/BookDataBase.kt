package fr.xebia.henripotier.book.data

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.xebia.henripotier.book.entity.BookEntity

@Database(entities = [BookEntity::class], version = 1)
abstract class BookDataBase : RoomDatabase() {
    abstract fun bookDao(): BookDao
}
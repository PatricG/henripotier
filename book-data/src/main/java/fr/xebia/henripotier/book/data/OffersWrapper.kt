package fr.xebia.henripotier.book.data

import fr.xebia.henripotier.book.model.Offer

data class OfferWrapper(
    val offers: List<Offer>
)

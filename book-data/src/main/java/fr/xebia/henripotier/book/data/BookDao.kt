package fr.xebia.henripotier.book.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import fr.xebia.henripotier.book.entity.BookEntity

@Dao
interface BookDao {

    @Query("SELECT * FROM book")
    fun getAll(): LiveData<List<BookEntity>>

    @Query("SELECT * FROM book WHERE isbn IN (:bookIds)")
    fun loadAllByIds(bookIds: IntArray): LiveData<List<BookEntity>>

    @Insert
    fun insertAll(vararg books: BookEntity)

    @Delete
    fun delete(book: BookEntity)
}
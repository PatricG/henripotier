package fr.xebia.henripotier.book.data
import fr.xebia.henripotier.book.model.Book
import fr.xebia.henripotier.book.model.Offer
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface BookService {

    @GET("books")
    suspend fun getBooks(): Response<List<Book>>

    @GET("books/{isbns}/commercialOffers")
    suspend fun getCommercialOffers(@Path("isbns") isbns : String): Response<OfferWrapper>
}

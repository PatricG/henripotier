package fr.xebia.henripotier.book.data

import fr.xebia.henripotier.book.model.Book
import fr.xebia.henripotier.book.model.Offer
import fr.xebia.henripotier.book.repository.BookRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class BookRepositoryImpl(private val bookService: BookService,
                                  private val bookDataBase: BookDataBase) : BookRepository {

    private val saveBooks = arrayListOf<Book>()

    private val selectedBookIsbn = ArrayList<String>()

    override fun getBooks(): Flow<List<Book>> = flow{
        val books = bookService.getBooks().body()!!
        saveBooks.addAll(books)
        emit(books)
    }

    override fun selectBookIsbn(isbn: String) {
        selectedBookIsbn.add(isbn)
    }

    override fun getSelectBook(): List<Book> = saveBooks.filter {
        selectedBookIsbn.contains(it.isbn)
    }

    override fun getSelectBookIsbn(): List<String> = selectedBookIsbn

    override fun unSelectBookIsbn(isbn: String) {
        selectedBookIsbn.remove(isbn)
    }

    override fun getCommercialOffers(isbns: List<String>): Flow<List<Offer>> = flow {
        val query = isbns.joinToString(",")
        val offers = bookService.getCommercialOffers(query).body()!!.offers
        emit(offers)
    }
}